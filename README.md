# macbook setup

All things to install on MacBook


# Initial tools

```
mkdir -p ~/.ssh
touch ~/.ssh/config
xcode-select --install
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> /Users/maksym/.zprofile
eval "$(/opt/homebrew/bin/brew shellenv)"
brew tap homebrew/cask-versions
brew install --cask google-chrome docker iterm2 visual-studio-code dropbox spotify tunnelblick
```

# Terminal

Tree and ack [explanations](https://sourabhbajaj.com/mac-setup/iTerm/)

```
brew install zsh zsh-completions tree ack fzf jq zsh-autosuggestions zsh-syntax-highlighting wireguard-tools wget gnu-sed thefuck fuzzy-find git-crypt 

# use Ctrl-R to get fuzzy-find search
$(brew --prefix)/opt/fzf/install

# correct spelling mistakes by running "fuck" after typo
echo 'eval $(thefuck --alias)' >> ~/.zshrc

# use gsed as sed on mac (sed from mac is annoying)
echo 'export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"' >> ~/.zshrc

echo 'export SHELL=`which zsh`' > .bash_profile
echo '[ -z "$ZSH_VERSION" ] && exec "$SHELL" -l' >> .bash_profile
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting ~/.zsh/zsh-syntax-highlighting
echo 'source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh' >> ~/.zshrc
echo 'source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >> ~/.zshrc
echo 'ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets cursor root)'  >> ~/.zshrc
echo 'ZSH_AUTOSUGGEST_USE_ASYNC=true'  >> ~/.zshrc
echo 'ZSH_AUTOSUGGEST_STRATEGY=(history completion)'  >> ~/.zshrc

# PROMPT config https://github.com/Powerlevel9k/powerlevel9k/blob/master/README.md#available-prompt-segments
cat <<EOF >> ~/.zshrc
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
DEFAULT_USER=maksym
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir dir_writable vcs kubecontext)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator public_ip disk_usage ram load time)
POWERLEVEL9K_STATUS_OK=false
POWERLEVEL9K_SHORTEN_DIR_LENGTH=1
POWERLEVEL9K_SHORTEN_DELIMITER=""
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_to_unique"
EOF

# Fix completions
sed -i '1iZSH_DISABLE_COMPFIX=true' ~/.zshrc

# styling https://gist.github.com/kevin-smets/8568070
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
sed -i 's/ZSH_THEME=.*/ZSH_THEME=\"powerlevel9k\/powerlevel9k\"/' ~/.zshrc
curl -o dark-solarized-patched.itermcolors https://raw.githubusercontent.com/mbadolato/iTerm2-Color-Schemes/master/schemes/Solarized%20Dark%20-%20Patched.itermcolors
open dark-solarized-patched.itermcolors
wget https://github.com/powerline/fonts/raw/master/SourceCodePro/Source%20Code%20Pro%20for%20Powerline.otf
open -a Font\ Book Source\ Code\ Pro\ for\ Powerline.otf
# click install font
# Then, import default profile into iterm2

# Set up language settings for different tools
sed -i '1iexport LANG=en_US.UTF-8' ~/.zshrc
sed -i '1iexport LC_ALL=en_US.UTF-8' ~/.zshrc

# Set editor for kubectl or other edits
echo 'export EDITOR=nano' >> ~/.zshrc

# CLI tools versions manager
brew install asdf
echo -e "\n. $(brew --prefix asdf)/asdf.sh" >> ~/.zshrc

# for asdf plugins
brew install \
  coreutils automake autoconf openssl \
  libyaml readline libxslt libtool unixodbc \
  unzip curl
  
asdf plugin-add kops https://github.com/Antiarchitect/asdf-kops.git

```

# Visual Studio Code

Install [this](https://github.com/shanalikhan/code-settings-sync) plugin and use it to sync settings from github

# Languages

## GO

```
cat <<EOF >> .zshrc
export GOPATH="\${HOME}/.go"
export GOROOT="\$(brew --prefix golang)/libexec"
export PATH="\$PATH:\${GOPATH}/bin:\${GOROOT}/bin"
test -d "\${GOPATH}" || mkdir "\${GOPATH}"
test -d "\${GOPATH}/src/github.com" || mkdir -p "\${GOPATH}/src/github.com"
EOF
brew install go
source .zshrc
go get -u golang.org/x/lint/golint
go get -u golang.org/x/lint/godoc
```

## Nodejs, yarn and snyk 

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | zsh
nvm install stable
brew install yarn --without-node
yarn global add snyk
```
# Gcloud

https://cloud.google.com/sdk/docs/downloads-interactive#mac

```
gcloud alpha
gcloud beta
```

# AWS
```
curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
sudo installer -pkg AWSCLIV2.pkg -target /
echo 'autoload bashcompinit && bashcompinit' >> ~/.zshrc
echo "complete -C $(which aws_completer) aws" >> ~/.zshrc
# https://docs.aws.amazon.com/cli/latest/userguide/cliv2-migration.html#cliv2-migration-output-pager
echo 'export AWS_PAGER=""' >> ~/.zshrc
```

# Kubernetes tools

```
brew tap garethr/kubeval
brew install kubeval kubernetes-cli helm@2 kubectx skaffold stern kops kustomize
brew link --overwrite kubernetes-cli
echo 'export PATH="/usr/local/opt/helm@2/bin:$PATH"' >> ~/.zshrc
echo 'source <(kubectl completion zsh)' >> .zshrc
echo 'source <(helm completion zsh)' >> .zshrc
echo 'source <(kops completion zsh)' >> .zshrc
kubectl completion zsh > "${fpath[1]}/_kubectl"
source ~/.zshrc
helm init --client-only
helm plugin install https://github.com/databus23/helm-diff --version master
helm plugin install https://github.com/mumoshu/helm-x
echo 'source <(stern --completion=zsh)' >> ~/.zshrc

# install krew plugin manager
(
  set -x; cd "$(mktemp -d)" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
  tar zxvf krew.tar.gz &&
  KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
  "$KREW" install --manifest=krew.yaml --archive=krew.tar.gz &&
  "$KREW" update
)
echo 'export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"' >> ~/.zshrc
source ~/.zshrc
kubectl krew install access-matrix view-secret modify-secret resource-capacity
```

# Other devops tools

[terraform](https://www.terraform.io/downloads.html)

```
brew tap wata727/tflint
brew install python tflint ansible hadolint pipenv terraform terraform-docs pre-commit
terraform -install-autocomplete
```

# Python and tesseract

```
brew install pipenv tesseract
```


# Android

[android-sdk-mac]()

```
# launch android studio and go through initial setup which installs android-sdk
# download flutter from https://flutter.dev/docs/get-started/install/macos#get-the-flutter-sdk
unzip ~/Downloads/flutter_macos_v1.5.4-hotfix.2-stable.zip
echo 'export PATH=$PATH:~/flutter/bin' >> ~/.zshrc
source ~/.zshrc
flutter doctor --android-licenses

# update signing key for new laptop in firebase https://developers.google.com/android/guides/client-auth
keytool -list -v -alias androiddebugkey -keystore ~/.android/debug.keystore

# then add it to firebase under general settings/your app > Add fingerprint. You need SHA-1 from the keytool output
```

# Pi-hole + wireguard

[general steps](https://www.ckn.io/blog/2017/11/14/wireguard-vpn-typical-setup/)

[pi-hole specific dns](https://docs.pi-hole.net/guides/unbound/)
